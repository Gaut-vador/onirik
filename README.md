#####Onirik#####

Cette application a pour but de permettre la rédation et la sauvegarde de rêves. Elle dispose d'un système de tags permettant de comptabiliser le nombre de rêve sur un même sujet, ainsi que de retrouver ces rêves en cliquant dessus.

Cette application fonctionne via des servlet et à été pensée pour fonctionner sur un Tomcat 8. La BDD est gérée avec SQLite 3. Pour plus d'informations sur la configuration du Tomcat reportez vous aux fichier CONFIG.md.

Il sera necessaire de recompiler le projet avec les bibliotheques contenues dans le dossier lib/.