var isEditingMode = false;


var editBtn;
var submitBtn;
var cancelBtn;

function initBtn()
{
	initEditBtn();
	initsubmitBtn();
	initCancelBtn();
}

function initEditBtn()
{
	editBtn = document.getElementById("editBtn");
	editBtn.addEventListener("click", turnToEditMode);
	console.log("initEditBtn");
}

function initCancelBtn()
{
	cancelBtn = document.getElementById("cancelBtn");
	cancelBtn.addEventListener("click", doCancelAction);
	console.log("initCancelBtn");
}

function initsubmitBtn()
{
	submitBtn = document.getElementById("submitBtn");
	console.log("initsubmitBtn");
}

function turnToEditMode()
{
 	editBtn.style.visibility = "hidden";
 	submitBtn.style.visibility = "visible";
 	toggleEditingMode();

}


function toggleEditingMode()
{
	document.getElementById("inputTags").readOnly    = isEditingMode;
	document.getElementById("inputDate").readOnly    = isEditingMode;
	document.getElementById("inputContent").readOnly = isEditingMode;
	isEditingMode = !isEditingMode;
}

function doCancelAction()
{
	if (isEditingMode)
	{
		editBtn.style.visibility = "visible";
 		submitBtn.style.visibility = "hidden";
 		toggleEditingMode();
	} else {
		window.location = "http://localhost:8080/Onirik/Menu";
	}
}