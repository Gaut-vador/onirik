package manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import types.DreamHeaderType;
import types.DreamType;
import utils.Logger;

public class DreamManager
{

	public static List<DreamHeaderType> getAllDreamHeader()
	{
		List<DreamHeaderType> result = new ArrayList<DreamHeaderType>();

		String querry = "SELECT * FROM DREAM_HEADER ORDER BY DATE DESC;";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}
			DreamHeaderType dreamHeader;

			while (resultSet != null && resultSet.next())
			{
				dreamHeader = new DreamHeaderType();
				dreamHeader.setId(resultSet.getInt("ID"));
				dreamHeader.setDreamDate(resultSet.getString("DATE"));
				dreamHeader.setTags(resultSet.getString("TAGS"));
				result.add(dreamHeader);

			}
		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
		return result;
	}

	public static List<DreamHeaderType> getDreamHeaderByDate(String pDate)
	{
		List<DreamHeaderType> result = new ArrayList<DreamHeaderType>();

		String querry = "SELECT * FROM DREAM_HEADER WHERE DATE LIKE '%" + pDate + "%';";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}
			DreamHeaderType dreamHeader;

			while (resultSet != null && resultSet.next())
			{
				dreamHeader = new DreamHeaderType();
				dreamHeader.setId(resultSet.getInt("ID"));
				dreamHeader.setDreamDate(resultSet.getString("DATE"));
				dreamHeader.setTags(resultSet.getString("TAGS"));
				result.add(dreamHeader);

			}
		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
		return result;
	}

	public static List<DreamHeaderType> getDreamHeaderByTag(String pTag)
	{
		List<DreamHeaderType> result = new ArrayList<DreamHeaderType>();

		String querry = "SELECT * FROM DREAM_HEADER WHERE TAGS LIKE '" + pTag + "';";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}
			DreamHeaderType dreamHeader;

			while (resultSet != null && resultSet.next())
			{
				dreamHeader = new DreamHeaderType();
				dreamHeader.setId(resultSet.getInt("ID"));
				dreamHeader.setDreamDate(resultSet.getString("DATE"));
				dreamHeader.setTags(resultSet.getString("TAGS"));
				result.add(dreamHeader);

			}
		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
		return result;
	}

	public static void insertNewDream(DreamType pDream)
	{

		DatabaseConnectionManager dbConnectionManager = null;
		String querryHeader = "INSERT INTO DREAM_HEADER (DATE, TAGS) VALUES (?,?);";
		String querryFullDream = "INSERT INTO DREAM (DATE, TAGS, CONTENT) VALUES (?,?,?);";
		try
		{
			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();

			Object[] argsHeader = { pDream.getDreamDate(), pDream.getTags() };
			dbConnectionManager.executeQuerry(querryHeader, argsHeader);

			String[] argsFull = { pDream.getDreamDate(), pDream.getTags(), pDream.getContent() };
			dbConnectionManager.executeQuerry(querryFullDream, argsFull);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}

	}

	public static DreamType getDream(int pId)
	{
		DreamType result = null;
		String querry = "SELECT * FROM DREAM WHERE ID=" + pId + ";";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}
			if (resultSet != null && resultSet.next())
			{
				result = new DreamType();
				result.setId(resultSet.getInt("ID"));
				result.setDreamDate(resultSet.getString("DATE"));
				result.setTags(resultSet.getString("TAGS"));
				result.setContent(resultSet.getString("CONTENT"));
			}
		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
		return result;
	}

	public static String[] getTags(int pDreamId)
	{
		String[] result = null;
		String querry = "SELECT TAGS FROM DREAM WHERE ID=" + pDreamId + ";";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}
			if (resultSet != null && resultSet.next())
			{
				result = resultSet.getString("TAGS").replaceAll(" ", "").split(";");
			}
		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
		return result;
	}

	public static void updateDream(DreamType pDream)
	{
		DatabaseConnectionManager dbConnectionManager = null;
		String querryHeader = "UPDATE DREAM_HEADER SET DATE = ?, TAGS = ? WHERE ID = ?;";
		String querryFullDream = "UPDATE DREAM SET DATE = ?, TAGS = ?, CONTENT=? WHERE ID = ?;";
		try
		{
			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();

			Object[] argsHeader = { pDream.getDreamDate(), pDream.getTags(), pDream.getId() };
			dbConnectionManager.executeQuerry(querryHeader, argsHeader);

			Object[] argsFull = { pDream.getDreamDate(), pDream.getTags(), pDream.getContent(), pDream.getId() };
			dbConnectionManager.executeQuerry(querryFullDream, argsFull);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
	}

	public static void deleteDream(int pDreamId)
	{
		DatabaseConnectionManager dbConnectionManager = null;

		String querryHeader = "DELETE FROM DREAM_HEADER WHERE ID = ?;";
		String querryFullDream = "DELETE FROM DREAM WHERE ID = ?;";
		try
		{
			DreamType dream = getDream(pDreamId);
			TagsManager.removeTagsOccurences(dream.getTags().split(";"));

			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();

			Object[] argsHeader = { pDreamId };
			dbConnectionManager.executeQuerry(querryHeader, argsHeader);

			Object[] argsFull = { pDreamId };
			dbConnectionManager.executeQuerry(querryFullDream, argsFull);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
	}
}
