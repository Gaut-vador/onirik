package manager;

import java.util.List;

import types.DreamHeaderType;
import types.TagType;
import types.enums.MenuSearchEnum;
import types.enums.TagsSearchMode;
import utils.DateUtils;
import utils.Logger;

public class MenuManager
{
	public static String getPrintableDreamList(MenuSearchEnum pSearchMode, String pOption)
	{
		String result = "<table>\n";
		List<DreamHeaderType> dreamHeaderList = null;
		switch (pSearchMode)
		{
		case ALL:
			dreamHeaderList = DreamManager.getAllDreamHeader();
			break;
		case DATE:
			dreamHeaderList = DreamManager.getDreamHeaderByDate(pOption);
			break;
		case TAGS:
			dreamHeaderList = DreamManager.getDreamHeaderByTag(pOption);
			break;
		default:
			return "<div> <p> Aucun r&ecirc;ve a afficher </p> </div>";
		}

		if (dreamHeaderList == null || dreamHeaderList.isEmpty())
		{
			return "<div> <p> Aucun r&ecirc;ve a afficher </p> </div>";
		}

		for (DreamHeaderType dreamHeader : dreamHeaderList)
		{
			result += buildHTMLArray(dreamHeader);
		}
		return result + "</table>\n";
	}

	private static String buildHTMLArray(DreamHeaderType pDreamHeader)
	{
		String result = "";
		//@formatter:off
		result += "<tr id=\"mainTableRow\"> \n" 
				  + "  <td>"
				  + "    <a href=\"/Onirik/Menu?date=" + pDreamHeader.getDreamDate() + "\">" + DateUtils.frenchFormatDate(pDreamHeader.getDreamDate()) + "</a>"
				  + "  </td>\n" 
				  + "  <td>";
		//@formatter:on
		String[] tags = pDreamHeader.getTags().replaceAll(" ", "").split(";");

		if (tags != null && tags.length > 0)
		{
			for (int i = 0; i < tags.length; i++)
			{
				if (i < tags.length - 1)
				{
					result += "  <a href=\"/Onirik/Menu?tag=" + tags[i] + "\">" + tags[i] + " / </a>";
				} else
				{
					result += "  <a href=\"/Onirik/Menu?tag=" + tags[i] + "\">" + tags[i] + "</a>";
				}
			}
		}
		//@formatter:off
		result += "</td>\n" 
				  + "  <td>"
				  			//lecture du reve
				  + "    <a href=\"/Onirik/ReadDream?id=" + pDreamHeader.getId() + "\"> <img class=\"iconDocument\" src=\"ressources/document_icon.png\" alt=\"Visionnage du r&ecirc;ve\"></a>"
				  			//Supression du reve
				  + "    <a href=\"/Onirik/deleteDreamManager?id=" + pDreamHeader.getId() + "\"> <img class=\"iconTrash\" src=\"ressources/trash_icon.png\" alt=\"Suppression du r&ecirc;ve\"></a>"
				  + "  </td>\n"
				  
				  + "</tr>\n";
		//@formatter:on
		return result;
	}

	public static String getPrintableTagsList(TagsSearchMode pSearchMode)
	{
		String result = "";
		try
		{
			List<TagType> tagsList;
			// TODO variabiliser TopTag/tout les tags
			switch (pSearchMode)
			{
			case TOP_TEN:
				result += "<h2 id=\"topTagTitle\"> Top Tags</h2> <p>";
				tagsList = TagsManager.get10MostUsedTags();
				break;
			case ALL:
				result += "<p>";
				tagsList = TagsManager.getAllTags();
				break;
			default:
				tagsList = TagsManager.getAllTags();
				break;
			}

			for (TagType tag : tagsList)
			{
				if (tag != null)
				{
					result += "<a href=\"/Onirik/Menu?tag=" + tag.getTagName() + "\">" + tag.toString() + "</a><br/>";
				}
			}

			result += "</p>";
			if (pSearchMode.equals(TagsSearchMode.TOP_TEN))
			{
				result += "<a href=\"/Onirik/Tags\"> Afficher plus de tags ...</a>";
			}
		} catch (Exception e)
		{
			Logger.writeException(e);
		}
		return result;
	}

}
