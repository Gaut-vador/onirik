package manager;

import manager.DreamManager;
import types.DreamType;

public class ReadDreamManager
{

	public static DreamType getDream(int pId)
	{
		return DreamManager.getDream(pId);
	}
}
