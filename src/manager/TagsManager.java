package manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import types.TagType;
import utils.Logger;

public class TagsManager
{

	final static int NUMBER_OF_TOP_TAGS = 10;

	public static List<TagType> getAllTags()
	{
		List<TagType> result = new ArrayList<TagType>();
		String querry = "SELECT * FROM TAGS ORDER BY COUNT DESC;";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}

			while (resultSet != null && resultSet.next())
			{
				result.add(new TagType(resultSet.getString("TAG_NAME"), resultSet.getInt("COUNT")));
			}

		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}

		}
		return result;
	}

	public static List<TagType> get10MostUsedTags()
	{
		List<TagType> result = new ArrayList<TagType>();
		String querry = "SELECT * FROM TAGS ORDER BY COUNT DESC LIMIT " + NUMBER_OF_TOP_TAGS + ";";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}

			while (resultSet != null && resultSet.next())
			{
				result.add(new TagType(resultSet.getString("TAG_NAME"), resultSet.getInt("COUNT")));
			}

		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}

		}
		return result;
	}

	public static void insertTags(String pTags)
	{
		List<String> tags = verifyTagUnicity(pTags.replaceAll(" ", "").split(";"));
		for (String tag : tags)
		{
			addTagOccurence(tag);
		}
	}

	private static ArrayList<String> verifyTagUnicity(String[] pTags)
	{
		HashSet<String> tagsSet = new HashSet<String>();
		for (String tag : pTags)
		{
			tagsSet.add(tag);
		}
		return new ArrayList<String>(tagsSet);

	}

	public static void editTags(int pDreamId, String pTags)
	{
		List<String> editedTags = verifyTagUnicity(pTags.replaceAll(" ", "").split(";"));
		List<String> originalTags = Arrays.asList(DreamManager.getTags(pDreamId));
		// tags a ajouter
		for (String tag : editedTags)
		{
			if (!originalTags.contains(tag))
			{
				addTagOccurence(tag);
			}
		}

		// tags a supprimer
		for (String tag : originalTags)
		{
			if (!editedTags.contains(tag))
			{
				removeTagOccurence(tag);
			}
		}
	}

	public static void removeTagsOccurences(String[] pTags)
	{
		List<String> tags = verifyTagUnicity(pTags);
		for (String tag : tags)
		{
			removeTagOccurence(tag);
		}
	}

	private static void removeTagOccurence(String pTag)
	{
		int nbOccurences = getTagOccurence(pTag);
		if (nbOccurences > 1)
		{
			updateTag(pTag, nbOccurences - 1);
		} else
		{
			deleteTag(pTag);
		}

	}

	private static void deleteTag(String pTag)
	{
		String querry = "DELETE FROM TAGS WHERE TAG_NAME = ?;";

		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();
			Object[] argsHeader = { pTag };
			dbConnectionManager.executeQuerry(querry, argsHeader);

		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}

		}
	}

	private static void addTagOccurence(String pTag)
	{

		int nbOccurences = getTagOccurence(pTag);
		if (nbOccurences > -1)
		{
			updateTag(pTag, nbOccurences + 1);
		} else
		{
			createTag(pTag);
		}
	}

	private static int getTagOccurence(String pTag)
	{
		String querry = "SELECT * FROM TAGS WHERE TAG_NAME = '" + pTag + "';";
		ResultSet resultSet = null;
		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			try
			{
				dbConnectionManager = DatabaseConnectionManager.getInstance();
				dbConnectionManager.connect();
				resultSet = dbConnectionManager.getStatement().executeQuery(querry);

			} catch (SQLException e)
			{
				Logger.writeException(e);
			}

			if (resultSet != null && resultSet.next())
			{
				return resultSet.getInt("COUNT");
			}

		} catch (SQLException e)
		{
			Logger.writeException(e);
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}

		}
		return -1;
	}

	private static void createTag(String pTag)
	{
		String querry = "INSERT INTO TAGS (TAG_NAME, COUNT) VALUES (?,?);";

		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();
			Object[] argsHeader = { pTag, 1 };
			dbConnectionManager.executeQuerry(querry, argsHeader);

		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}

		}
	}

	private static void updateTag(String pTag, int pCount)
	{
		String querry = "UPDATE TAGS SET COUNT = ? WHERE TAG_NAME = ?;";

		DatabaseConnectionManager dbConnectionManager = null;
		try
		{
			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();
			Object[] argsHeader = { pCount, pTag };
			dbConnectionManager.executeQuerry(querry, argsHeader);

		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}

		}
	}
}
