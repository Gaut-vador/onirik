package manager.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.DreamManager;
import manager.TagsManager;
import types.DreamType;

@WebServlet("/addDreamManager")
public class AddDreamManager extends HttpServlet
{

	private static final long serialVersionUID = -7626938451301898740L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if (request != null)
		{
			TagsManager.insertTags(request.getParameter("dreamTags"));
			DreamType dream = new DreamType();
			dream.setDreamDate(request.getParameter("dreamDate"));
			dream.setTags(request.getParameter("dreamTags"));
			dream.setContent(request.getParameter("dream"));
			// Logger.logMessage(dream.toString());
			DreamManager.insertNewDream(dream);

		}
		response.sendRedirect("/Onirik/Menu");
	}

}
