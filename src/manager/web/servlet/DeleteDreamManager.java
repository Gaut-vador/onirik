package manager.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.DreamManager;
import utils.Logger;

@WebServlet("/deleteDreamManager")
public class DeleteDreamManager extends HttpServlet
{

	private static final long serialVersionUID = 2498828445202457409L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if (request != null)
		{
			int dreamId = 0;
			try
			{
				dreamId = Integer.parseInt(request.getParameter("id"));
			} catch (NumberFormatException e)
			{
				Logger.writeException(e);
				response.sendRedirect("/Onirik/Menu");
			}
			DreamManager.deleteDream(dreamId);
		}
		// TODO Ajout de message de confirmation de suppression du reve
		response.sendRedirect("/Onirik/Menu");
	}
}
