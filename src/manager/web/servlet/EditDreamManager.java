package manager.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.DreamManager;
import manager.TagsManager;
import types.DreamType;
import utils.Logger;

@WebServlet("/editDreamManager")
public class EditDreamManager extends HttpServlet
{

	private static final long serialVersionUID = -7626938451301898740L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if (request != null)
		{
			int dreamId = 0;
			try
			{
				dreamId = Integer.parseInt(request.getParameter("dreamID"));
			} catch (NumberFormatException e)
			{
				Logger.writeException(e);
				response.sendRedirect("/Onirik/Menu");
			}
			DreamType originalDream = DreamManager.getDream(dreamId);
			if (originalDream != null)
			{
				TagsManager.editTags(dreamId, request.getParameter("dreamTags"));

				DreamType dream = new DreamType();
				dream.setId(dreamId);
				dream.setDreamDate(request.getParameter("dreamDate"));
				dream.setTags(request.getParameter("dreamTags"));
				dream.setContent(request.getParameter("dream"));
				DreamManager.updateDream(dream);
			}
		}
		response.sendRedirect("/Onirik/Menu");
	}
}
