package types;

public class DreamHeaderType
{
	private int id;

	private String dreamDate;
	private String tags;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getDreamDate()
	{
		return dreamDate;
	}

	public void setDreamDate(String dreamDate)
	{
		this.dreamDate = dreamDate;
	}

	public String getTags()
	{
		return tags;
	}

	public void setTags(String tags)
	{
		this.tags = tags;
	}

	public String toString()
	{
		return "id: " + this.id + "; date: " + dreamDate + "; tags: " + tags;
	}
}
