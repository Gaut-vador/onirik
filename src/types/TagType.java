package types;

public class TagType
{
	private String tagName;
	private int occurences;
	
	
	public TagType()
	{
		
	}
	
	public TagType(String pName, int pOccurence)
	{
		setTagName(pName);
		setOccurences(pOccurence);
	}
	
	public String toString()
	{
		return this.tagName + " (" + this.occurences + ")";
	}
	
	public String getTagName()
	{
		return tagName;
	}
	public void setTagName(String tagName)
	{
		this.tagName = tagName;
	}
	public int getOccurences()
	{
		return occurences;
	}
	public void setOccurences(int occurences)
	{
		this.occurences = occurences;
	}
}
