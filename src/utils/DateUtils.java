package utils;

public class DateUtils
{
	public static String frenchFormatDate(String pDate)
	{
		if (pDate == null || pDate.isEmpty() || !pDate.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}"))
		{
			Logger.logMessage("La date :" + pDate + " ne corespond pas au format attendu (yyyy-mm-dd)");
			return "";
		}
		String[] formatter = pDate.split("-");
		return formatter[2] + "-" + formatter[1] + "-" + formatter[0];
	}
}
