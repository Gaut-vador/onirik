package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

public class Logger
{
	private static final String LOG_PATH = "Logs_Onirik" + File.separator;
	private static final String EXCEPTION_DETAILS_LOG_FILENAME = "ExceptionDetails.log";
	private static final String TRACE_LOG_FILENAME = "Trace.log";

	public Logger()
	{

	}

	public static void writeException(Exception pException)
	{

		FileWriter fileWriter = null;
		BufferedWriter output = null;
		try
		{
			fileWriter = getFile(EXCEPTION_DETAILS_LOG_FILENAME);
			output = new BufferedWriter(fileWriter);
			Date dateOfError = new Date();
			DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
			output.write(shortDateFormat.format(dateOfError) + " : " + pException.getMessage() + "\n");
			for (StackTraceElement stackTraceElement : pException.getStackTrace())
			{
				output.write("\t" + stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "(line: "
						+ stackTraceElement.getLineNumber() + ")\n");
			}
			output.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			if (fileWriter != null)
			{
				try
				{
					fileWriter.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			if (output != null)
			{
				try
				{
					output.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public static void logMessage(String pMessage)
	{
		FileWriter fileWriter = null;
		BufferedWriter output = null;
		try
		{
			fileWriter = getFile(TRACE_LOG_FILENAME);
			output = new BufferedWriter(fileWriter);
			output.write(pMessage + "\n");

			output.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			if (fileWriter != null)
			{
				try
				{
					fileWriter.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			if (output != null)
			{
				try
				{
					output.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private static FileWriter getFile(String pFileName)
	{
		try
		{
			File path = new File(LOG_PATH);
			if (!path.exists())
			{
				path.mkdirs();
			}
			File file = new File(LOG_PATH + pFileName);
			if (!file.exists())
			{
				file.createNewFile();
			}

			return new FileWriter(file, true);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;

	}
}
