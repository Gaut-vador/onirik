package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/AddDream")
public class AddDreamView extends HttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8203241619427538057L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();

		out.println("<!DOCTYPE html>\n" + "<html lang=\"fr\">\n" + "<head>\n" + "<meta charset=\"utf-8\">\n"
				+ "<meta content=\"IE=edge\" http-equiv=\"X-UA-Compatible\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleAdd.css\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleGlobal.css\"> \n"
				+ "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n"
				+ "<title>Ajout de r&ecirc;ve</title>");
		out.println("</head>");
		out.println("<body>");
		//@formatter:off
		out.println("<form id=\"addDream\" action=\"addDreamManager\" method=\"POST\">\n" + 
				"           Tags:\n" + 
				"           <input name=\"dreamTags\" type=\"text\" title=\"S&eacute;parez les tags avec un \';\'\">\n" +
				"           <br/>" +
				"           Date du r&ecirc;ve:\n" + 
				"           <input name=\"dreamDate\" type=\"date\">\n" +
				"           <br/>" +
				"           <textarea name=\"dream\">The cat was playing in the garden.</textarea>\n "+
				"            <div id=\"buttons\">\n"+
				"				<button type=\"submit\">Enregistrer le r&ecirc;ve</button>\n" + 
				"				<a href=\"/Onirik/Menu\">Annuler</a>\n" + 
				"           </div>\n"+
				"			</form>");
		
		//@formatter:on
		out.println("</body></html>");
	}
}
