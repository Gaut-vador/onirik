package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.MenuManager;
import types.enums.MenuSearchEnum;
import types.enums.TagsSearchMode;

@WebServlet("/Menu")
public class MenuView extends HttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8203241619427538057L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();

		out.println("<!DOCTYPE html>\n" + "<html lang=\"fr\">\n" + "<head>\n" + "<meta charset=\"utf-8\">\n"
				+ "<meta content=\"IE=edge\" http-equiv=\"X-UA-Compatible\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleGlobal.css\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleMenu.css\"> \n"
				+ "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n" + "<title>Menu</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1><a href=\"/Onirik/Menu\">Onirik</a></h1>");
		
		//Paneau principal : liste des reves
		
		out.println("<div id=\"mainPanel\">");
		
		if(request !=null)
		{
			String printableDreamList ="";
			if (request.getParameter("date") != null)
			{
				printableDreamList = MenuManager.getPrintableDreamList(MenuSearchEnum.DATE, request.getParameter("date"));
			}
			else if (request.getParameter("tag") != null)
			{
				printableDreamList = MenuManager.getPrintableDreamList(MenuSearchEnum.TAGS, request.getParameter("tag"));
			}
			else
			{
				printableDreamList = MenuManager.getPrintableDreamList(MenuSearchEnum.ALL, null);
			}

			out.println(printableDreamList);
		}
		else
		{
			out.println("<p class=\"error\"> ERROR </p>");
			//TODO Logger exeption
		}
		
		out.println("</div>");
		
		// Paneau latéral : liste des tags
		
		out.println("<div id=\"tagsMenu\">");
		String tagsList = MenuManager.getPrintableTagsList(TagsSearchMode.TOP_TEN);
		
		out.println(tagsList);
		out.println("</div>");
		

		out.println("<a id=\"addButton\" href=\"/Onirik/AddDream\" role=\"button\">+</a>");


		out.println("</body></html>");
	}
}
