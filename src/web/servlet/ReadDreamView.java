package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.ReadDreamManager;
import types.DreamType;

@WebServlet("/ReadDream")
public class ReadDreamView extends HttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5167541349587724983L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();

		out.println("<!DOCTYPE html>\n" + "<html lang=\"fr\">\n" + "<head>\n" + "<meta charset=\"utf-8\">\n"
				+ "<meta content=\"IE=edge\" http-equiv=\"X-UA-Compatible\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleEdit.css\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleGlobal.css\"> \n"
				+ "<script src=\"/Onirik/js/editDream.js\"></script>\n"
				+ "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n"
				+ "<title>Lecteur de r&ecirc;ve</title>");
		out.println("</head>");
		out.println(" <body onload=\"initBtn()\"> ");
		if (request != null && request.getParameter("id") != null)
		{

			int id = -1;

			try
			{
				id = Integer.parseInt(request.getParameter("id"));
			} catch (NumberFormatException e)
			{
				out.println("<p class=\"error\"> ERROR:<br/>id de reve invalide </p>");
			}
			if (id > -1)
			{
				DreamType dream = ReadDreamManager.getDream(id);
				if (dream == null)
				{
					out.println("<p class=\"error\"> ERROR:<br/>Reve introuvable </p>");
				} else
				{
					//@formatter:off
					out.println("<form id=\"addDream\" action=\"editDreamManager\" method=\"POST\">\n"
							+ "				ID du r&ecirc;ve:\n" 
							+ "           	<input name=\"dreamID\" id=\"inputId\" type=\"text\" value=\"" + dream.getId() + "\" readonly>\n" 
							+ "           	<br/>\n"  
							+ "           	Tags:\n"
							+ "           	<input name=\"dreamTags\" id=\"inputTags\" type=\"text\" value=\"" + dream.getTags() + "\" title=\"S&eacute;parez les tags avec un \';\'\" readonly>\n" 
							+ "           	<br/>\n"
							+ "           	Date du r&ecirc;ve:\n"
							+ "           	<input name=\"dreamDate\" id=\"inputDate\" type=\"date\"  value=\"" + dream.getDreamDate() + "\" readonly>\n" 
							+ "           	<br/>\n" 
							+ "           	<textarea name=\"dream\" id=\"inputContent\" readonly>" + dream.getContent() + "</textarea>\n " 
							+ "            <div id=\"buttons\">\n"
							+ "					<button type=\"submit\" id=\"submitBtn\">Enregistrer le r&ecirc;ve</button>\n"
							+ "					<button type=\"button\" id=\"editBtn\">Editer le r&ecirc;ve</button>\n"
							+ "					<button type=\"button\" id=\"cancelBtn\">Annuler</button>\n"
							+ "           	</div>\n" 
							+ "			</form>\n");
					//formatter:on
				}
			}
		} else
		{
			out.println("<p class=\"error\"> ERROR:<br/>ID de reve manquant </p>");
		}

		out.println("</body></html>");
	}
}
