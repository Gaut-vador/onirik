package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.MenuManager;
import types.enums.TagsSearchMode;

@WebServlet("/Tags")
public class TagsView extends HttpServlet
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2146720966911656419L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();

		out.println("<!DOCTYPE html>\n" + "<html lang=\"fr\">\n" + "<head>\n" + "<meta charset=\"utf-8\">\n"
				+ "<meta content=\"IE=edge\" http-equiv=\"X-UA-Compatible\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleGlobal.css\"> \n"
				+ "<link rel=\"stylesheet\" href=\"/Onirik/css/styleTag.css\"> \n"
				+ "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n" + "<title>Menu</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1><a href=\"/Onirik/Menu\">Onirik</a></h1>");
		
		
		out.println("<div id=\"tagList\">");
		String tagsList = MenuManager.getPrintableTagsList(TagsSearchMode.ALL);
		out.println(tagsList);
		
		out.println("</div>");
		out.println("<a id=\"addButton\" href=\"/Onirik/AddDream\" role=\"button\">+</a>");


		out.println("</body></html>");
	}
}
